class PullRequestRepository:
    def __init__(self, connection):
        self.database_connection = connection

    def save_pull_request(self, pull_request_data):
        with self.database_connection.cursor() as cursor:
            cursor.execute("INSERT into PULL_REQUEST "
                           "(id, repo_id, title, state, url, created_at, updated_at, "
                           "closed_at,author)"
                           " VALUES (%s,%s, %s,%s, %s, %s,%s, %s, %s)"
                           " ON CONFLICT DO NOTHING",
                           (pull_request_data['id'],
                            pull_request_data['repo_id'],
                            pull_request_data['title'],
                            pull_request_data['state'],
                            pull_request_data['url'],
                            pull_request_data['created_at'],
                            pull_request_data['updated_at'],
                            pull_request_data["closed_at"],
                            pull_request_data['author']))
            # On conflict - probably need to update in this case.

    def fetch_pull_request(self, id):
        with self.database_connection.cursor() as cursor:
            cursor.execute("SELECT * from PULL_REQUEST where id = %s", (id,))
            return cursor.fetchone()
