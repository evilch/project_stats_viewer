import logging

from external.datasources.clients.client_github import format_pull_request_url




class PullRequestImport:

    logger = logging.getLogger(__name__)

    def __init__(self, database_connection, git_client):
        self.database_connection = database_connection
        self.git_client = git_client

    #TODO: Add pagination
    def import_pull_requests(self, host, org, repo, params=None):
        self.logger.info("loading pull requests")
        if(params is None):
            params = {}
        params["state"] = "all"
        self.git_client.load_with_paging(format_pull_request_url(host, org, repo, params),
                           lambda release_data: self.database_connection.save_pull_request(
                               map_github_pull_request_to_internal_model(release_data, repo)))
        self.logger.info("Loaded pull requests")


def map_github_pull_request_to_internal_model(pull_request, repo_name):
    return {
        "id": pull_request["id"],
        "repo_id":  repo_name,
        "title": pull_request["title"],
        "state": pull_request["state"],
        "url": pull_request["url"],
        "created_at": pull_request["created_at"],
        "updated_at": pull_request["updated_at"],
        "closed_at": pull_request["closed_at"],
        "author":pull_request["user"]["login"],
        "reviewers_count": len(pull_request["requested_reviewers"]),
        "assignees_count": len(pull_request["assignees"]),
    }
