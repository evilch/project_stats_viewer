import json
import logging

header_link_value = 'link'

logger = logging.getLogger(__name__)


class GitHubClient():
    def __init__(self, http_session=None):
        self.http_session = http_session

    def parse_response(self, http_response):
        return_value = {}
        if (http_response.headers is not None and header_link_value in http_response.headers):
            links = http_response.headers[header_link_value]
            refs = links.split(',')
            for ref in refs:
                link_and_state = ref.split(';')
                link = link_and_state[1].strip().split('rel="')[1].strip('"')
                ref_url = link_and_state[0].strip().strip('<').strip('>')
                return_value[link] = ref_url
        return_value['content'] = json.loads(http_response.content)
        return return_value

    def load_with_paging(self,  url,  save_method):
        while True:
            payload = self.call_url(url)

            for item in payload['content']:
                save_method(item)

            logger.info("Loaded {} commits, from url {}".format(len(payload['content']), url))

            if 'next' in payload:
                logger.info("Next is {} ".format(payload['next']))
                url = payload['next']
            else:
                break

    def call_url(self, url):
        logger.info("Calling url {}".format(url))
        res = self.http_session.get(url=url)
        return self.parse_response(res)


def format_commit_url(host, org, repo, params=None):
    return format_url(host, org, params, repo, "commits")


def format_release_url(host, org, repo, params=None):
    return format_url(host, org, params, repo, "releases")


def format_url(host, org, params, repo, type):
    url = "{}/{}/{}/{}".format(host, org, repo, type)
    if params:
        url += '?'
        for key, value in params.items():
            url += "{}={}&".format(key, value)
        url = url[:-1]  # remove trailing &
    return url


def format_pull_request_url(host, org, repo, params=None):
    return format_url(host, org, params, repo, "pulls")