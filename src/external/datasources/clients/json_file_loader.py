import json


class JsonFileImporter:
    def load_json_file(self, file_location):
        with open(file_location) as json_file:
            json_data = json_file.read()
            return json.loads(json_data)