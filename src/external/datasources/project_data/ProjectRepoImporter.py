class ProjectRepoImporter:

    def __init__(self, json_file_importer, project_repo_repository):
        self.json_file_importer = json_file_importer
        self.project_repo_repository = project_repo_repository

    def load_projects(self, file_location):
        json_file = self.json_file_importer.load_json_file(file_location)

        for project in json_file:
            self.project_repo_repository.save_project(project)

    def load_repos(self, file_location):
        json_file = self.json_file_importer.load_json_file(file_location)

        for repo in json_file:
            self.project_repo_repository.save_repo(repo)
