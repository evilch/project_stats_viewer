class ProjectRepoRepository:
    def __init__(self, connection):
        self.database_connection = connection

    def save_project(self, project_test_data):
       #Save the project data to the database
       with self.database_connection.cursor() as cursor:
           cursor.execute("INSERT into PROJECT "
                          "(id, name, description, url_to_details)"
                          " VALUES (%s,%s, %s,%s)"
                          " ON CONFLICT DO NOTHING",
                          (project_test_data['id'],
                           project_test_data['name'],
                           project_test_data['description'],
                           project_test_data['url_to_details']))

    def fetch_project(self, id):
        with self.database_connection.cursor() as cursor:
            cursor.execute("SELECT * from PROJECT where id = %s", (id,))
            return cursor.fetchone()

    def save_repo(self, repo_test_data):
        with self.database_connection.cursor() as cursor:
            cursor.execute("INSERT into REPOSITORY "
                           "(id, name, description, url_to_details, project_id)"
                           " VALUES (%s,%s, %s,%s, %s)"
                           " ON CONFLICT DO NOTHING",
                           (repo_test_data['id'],
                            repo_test_data['name'],
                            repo_test_data['description'],
                            repo_test_data['url_to_details'],
                            repo_test_data['project_id']))

    def fetch_repo(self, param):
        with self.database_connection.cursor() as cursor:
            cursor.execute("SELECT * from REPOSITORY where id = %s", (param,))
            return cursor.fetchone()

