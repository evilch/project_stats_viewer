import logging

from external.datasources.clients.client_github import format_commit_url


class GitHubImporter:
    logger = logging.getLogger(__name__)

    def __init__(self, git_database_handler, git_hub_client):
        self.git_hub_client = git_hub_client
        self.git_database_handler = git_database_handler

    def load_single_page_of_commits(self, host, org, repo):
        commits = self.fetch_commit(host, org, repo)
        for commit in commits:
            self.git_database_handler.map_and_save(commit, repo,org)

    def fetch_commit(self, host, org, repo):
        url = format_commit_url(host, org, repo)
        commits = self.git_hub_client.call_url(url)['content']
        return commits

    def load_commits_from_repo(self, host, org, repo):
        url = format_commit_url(host, org, repo)
        self.git_hub_client.load_with_paging(url,
                         lambda commit: self.git_database_handler.map_and_save(commit,repo,organization=org))



    def load_single_commit(self, url):
        content = self.git_hub_client.call_url(url)
        commit = content['content']
        self.logger.info("Loaded {} commit".format(commit))
        return commit

    def enhance_commits_from_repo(self, git_repo ):
        self.logger.info("Loading commits for {}".format(git_repo))
        date_from = self.git_database_handler.fetch_last_updated_date_for_commit(git_repo)
        database_commits = self.git_database_handler.fetch_commits_for_repo(git_repo, date_from)
        self.logger.info("Loaded {} commits".format(len(database_commits)))
        for loaded_commit in database_commits:
            self.logger.info("Enhanced commit {}".format(loaded_commit))
            commit = self.load_single_commit(loaded_commit[11])
            self.git_database_handler.save_extra_files(commit, git_repo)
        self.logger.info("Loaded commits for {}".format(git_repo))

