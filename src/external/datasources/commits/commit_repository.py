import logging


def check_if_key_is_present_else_zero(commit, key):
    if key in commit:
        return commit[key]
    else:
        return 0


class CommitRepository:
    logger = logging.getLogger(__name__)

    def __init__(self, database_connection):
        self.database_connection = database_connection

    def map_and_save(self, commit, repo=None, organization=None):
        try:
            with self.database_connection.cursor() as cursor:
                cursor.execute("INSERT into GIT_COMMIT "
                               "(sha,short_sha,git_repo,author_name,author_email,"
                               "message,created_at,updated_at,url,organization) "
                               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING",
                               (commit['sha'],
                                commit['sha'][0:7],
                                repo,
                                commit['commit']['author']['name'],
                                commit['commit']['author']['email'],
                                commit['commit']['message'],
                                commit['commit']['committer']['date'],
                                commit['commit']['author']['date'],
                                commit['url'],
                                organization))
        except Exception as e:
            self.logger.exception(e)
            self.logger.error("Transaction abandoned, no data saved, failing commit {}".format(commit['sha']))

    def save_extra_files(self, commit, repo):
        self.logger.info("Saving files for commit {}".format(commit))
        try:
            with self.database_connection.cursor() as cursor:
                self.logger.info("Updating commit {} stats {}".format(commit['sha'], commit['stats']))

                cursor.execute("UPDATE GIT_COMMIT set lines_added = %s , lines_removed = %s , lines_changed = %s "
                               "where sha = %s",
                                (check_if_key_is_present_else_zero(commit['stats'], 'additions'),
                                 check_if_key_is_present_else_zero(commit['stats'],'deletions'),
                                 check_if_key_is_present_else_zero(commit['stats'],'total'),
                                 commit['sha']))
                commit['files'].sort(key=lambda x: x['filename'])
                for file in commit['files']:
                    cursor.execute("INSERT into GIT_FILES values (%s,%s,%s,%s,%s,%s,%s)",
                                   (file['sha'],
                                    file['filename'],
                                    repo, commit['sha'],
                                   file['additions'],
                                   file['deletions'],
                                   file['changes']))

        except Exception as e:
            self.logger.exception(e)
            self.logger.error("Transaction abandoned, no data saved, failing commit {}".format(commit['sha']))

    def fetch_commits_for_repo(self, repo_name, date_from):
        with self.database_connection.cursor() as cursor:
            if date_from is None:
                cursor.execute("SELECT * from GIT_COMMIT where git_repo = %s", repo_name)
            else:
                cursor.execute("SELECT * from GIT_COMMIT where git_repo = %s and updated_at >= %s", (repo_name, date_from ))
            return cursor.fetchall()


    def fetch_last_updated_date_for_commit(self, param):
        with self.database_connection.cursor() as cursor:
            cursor.execute("SELECT MAX(updated_at) from GIT_COMMIT")
            return cursor.fetchone()