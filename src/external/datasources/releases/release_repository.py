class ReleaseRepository:
    def __init__(self, connection):
        self.connection = connection

    def save_release(self,  release):
        with self.connection.cursor() as cursor:
            cursor.execute("INSERT INTO RELEASE "
                           "(composite_id,"
                           "component_name, release_name, release_description, "
                           "release_date, source_system, author, link_to_details) "
                           "VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING",
                           (release["component_name"] + "-" + release["release_name"], release["component_name"], release["release_name"],
                            release["release_description"],
                            release["release_date"], release["source_system"], release["author"], release["link_to_details"]))

    def fetch_release(self, param):
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT * from RELEASE where composite_id = %s", (param,))
            return cursor.fetchone()