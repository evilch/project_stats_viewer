import logging

from external.datasources.clients.client_github import format_release_url


class ReleaseDataImport:
    logger = logging.getLogger(__name__)

    def __init__(self, database_connection, git_client):
        self.database_connection = database_connection
        self.git_client = git_client

    def import_releases(self, host, org, repo,params=None):
        self.logger.info("Loading releases")
        self.git_client.load_with_paging(format_release_url(host, org, repo, params),
                           lambda release_data: self.database_connection.save_release(
                               map_github_release_to_internal_model(release_data, repo)))
        self.logger.info("Finished releases")


def map_github_release_to_internal_model(release_data, component_name):
    return {
        "component_name": component_name,
        "release_name": release_data["name"],
        "release_description": release_data["body"],
        "release_date": release_data["published_at"],
        "source_system": "github",
        "author": release_data["author"]["login"]
    }
