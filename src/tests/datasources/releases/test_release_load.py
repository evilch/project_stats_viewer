import unittest
from unittest.mock import MagicMock

from external.datasources.clients.client_github import GitHubClient
from external.datasources.releases.release_import import ReleaseDataImport, map_github_release_to_internal_model
from tests.datasources.commits.mocks import request_mock_returns_200
from tests.datasources.releases.release_test_data import release_return, release_data_json




class ReleaseLoadTest(unittest.TestCase):

    internal_release = {
        "component_name": "Hello-World",
        "release_name": "v1.0.0",
        "release_description": "Description of the release",
        "release_date": "2013-02-27T19:35:32Z",
        "source_system": "github",
        "author": "octocat"
    }
    def test_load_releases_from_github(self):
        # Mock git hub client
        session = request_mock_returns_200(body=release_return)
        git_client = GitHubClient(session)
        database_connection_mock = MagicMock(name="Database Mock")
        release_importer = ReleaseDataImport(database_connection_mock, git_client)
        release_importer.import_releases("http://host:2003", "octocat", "Hello-World")
        session.get.assert_called_with(url="http://host:2003/octocat/Hello-World/releases")
        database_connection_mock.save_release.assert_called_with(self.internal_release)

    def test_map_github_release_to_internal_model(self):
        internal_model = map_github_release_to_internal_model(release_data_json[0], 'Hello-World')
        self.assertEqual(internal_model, self.internal_release)


if __name__ == '__main__':
    unittest.main()
