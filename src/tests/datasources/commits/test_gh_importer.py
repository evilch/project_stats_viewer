import unittest
from unittest.mock import MagicMock, call
import datetime
from external.datasources.commits.import_commits import GitHubImporter
from external.datasources.clients.client_github import GitHubClient
from external.datasources.commits.commit_repository import CommitRepository
from tests.datasources.commits.git_hub_test_data import response, full_response, single_commit
from tests.datasources.commits.mocks import database_cursor_mock, Response, request_mock_returns_200, response_object


class GitHubImporterTest(unittest.TestCase):



    def test_fetch_single_page_commits_from_github_and_store_in_db(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        git_database_handler = CommitRepository(database_connection_mock)

        session = request_mock_returns_200(body=response)
        git_client = GitHubClient(session)

        git_hub_import = GitHubImporter(git_database_handler, git_client)
        git_hub_import.load_single_page_of_commits("http://host:2003", "octocat", "Hello-World")
        session.get.assert_called_with(url="http://host:2003/octocat/Hello-World/commits")

        call1 = self.insert_call_to_database('CATb09b5b57875f334f61aebed695e2e4193db5e',
                                             'CATb09b', 'Monalisa Octocat')
        call2 = self.insert_call_to_database('DOGb09b5b57875f334f61aebed695e2e4193db5e', 'DOGb09b',
                                             'Monalisa Octocat 2')
        cursor_mock.execute.assert_has_calls([call1, call2])

    def test_enhance_multiple_commits_from_database(self):
        database_connection_mock = MagicMock(name="Database Mock")
        database_connection_mock.fetch_commits_for_repo.return_value = commit_return
        session = request_mock_returns_200(body=single_commit)
        git_client = GitHubClient(session)
        git_hub_import = GitHubImporter(database_connection_mock, git_client)
        git_hub_import.enhance_commits_from_repo("git_repo")
        database_connection_mock.fetch_last_updated_date_for_commit.assert_called()
        session.get.assert_has_calls([
            call(url="https://api.github.com/repos/DEFRA/adp-portal2/commits/4110888545b23f24f2cb20c8dcba3839b37fd0b9"),
            call(url="https://api.github.com/repos/DEFRA/adp-portal2/commits/54c7602f66c44f2860d016ccb3a38259ee3c9895"), ])

    # Helper functions
    def insert_call_to_database(self, full_sha, short_sha, author_username):
        database_call = call("INSERT into GIT_COMMIT "
                             "(sha,short_sha,git_repo,author_name,"
                             "author_email,"
                             "message,created_at,updated_at,"
                             "url,organization) "
                             "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING",
                             (full_sha, short_sha, 'Hello-World', author_username,
                              'support@github.com',
                              'Fix all the bugs', '2011-04-14T16:00:49Z', '2011-04-14T16:00:49Z',
                              'https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e',
                              'octocat'))
        return database_call



commit_return = [('4110888545b23f24f2cb20c8dcba3839b37fd0b9',
                  '4110888',
                  'James Coxhead',
                  'james.coxhead@capgemini.com',
                  'ci: bump templates version (#133)',
                  datetime.datetime(2024, 5, 24, 10, 4, 40),
                  datetime.datetime(2024, 5, 24, 10, 4, 40),
                  'adp-portal2',
                  1,
                  2,
                  3,
                  'https://api.github.com/repos/DEFRA/adp-portal2/commits/4110888545b23f24f2cb20c8dcba3839b37fd0b9',
                  'DEFRA'),
                 ('54c7602f66c44f2860d016ccb3a38259ee3c9895',
                 '54c7602',
                 'Jeevan Kuduva Ravindran',
                 '119388966+jeevankuduvaravindran@users.noreply.github.com',
                 '309469: ',
                 datetime.datetime(2024, 5, 23, 13, 26, 17),
                 datetime.datetime(2024, 5, 23, 13, 26, 17),
                 'adp-portal2',
                 1,
                 2,
                 3,
                 'https://api.github.com/repos/DEFRA/adp-portal2/commits/54c7602f66c44f2860d016ccb3a38259ee3c9895',
                 'DEFRA')]
