import datetime
import logging
import psycopg
from external.datasources.commits.commit_repository import CommitRepository
from tests.datasources.commits.git_hub_test_data import octacat_commit_as_json, octadog_commit_as_json, \
    single_commit_as_json, single_commit_no_updates_as_json
import unittest
from unittest.mock import patch, call
from psycopg import errors

from tests.datasources.commits.mocks import database_cursor_mock


class CommitDataBaseHandlerTest(unittest.TestCase):
    @patch("external.datasources.commits.commit_repository.CommitRepository.logger")
    def test_failing_transaction(self,logger_mock):

        cursor_mock, database_connection_mock = database_cursor_mock()
        cursor_mock.execute.side_effect = [None,psycopg.errors.UniqueViolation("CATb09b5b57875f334f61aebed695e2e4193db5e"),]

        git_database_connection = CommitRepository(database_connection_mock)
        git_database_connection.map_and_save(octadog_commit_as_json,"git_hub_repo", "org")
        git_database_connection.map_and_save(octacat_commit_as_json,"git_hub_repo","org")
        self.assertEqual(2, cursor_mock.execute.call_count)
        self.assertEqual(1, logger_mock.error.call_count)
        logger_mock.error.assert_called_with("Transaction abandoned, no data saved, failing commit CATb09b5b57875f334f61aebed695e2e4193db5e")


    def test_load_changed_files_for_commit(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        git_database_handler = CommitRepository(database_connection_mock)
        git_database_handler.save_extra_files(single_commit_as_json,"git_hub_repo")
        update_call = call("UPDATE GIT_COMMIT "
                           "set lines_added = %s , lines_removed = %s , lines_changed = %s "
                           "where sha = %s",
                           (63,17,80,'67f8beea7ba8f925442f1fbcb34bc9de6e6619ea'))

        add_files_call = call("INSERT into GIT_FILES values (%s,%s,%s,%s,%s,%s,%s)", ('0ec6a10af5ee944eb7bf7c51bedd7d7ebca95878', 'app/package.json', 'git_hub_repo', '67f8beea7ba8f925442f1fbcb34bc9de6e6619ea', 1, 1, 2))

        cursor_mock.execute.assert_has_calls([update_call,add_files_call])
        self.assertEqual(6, cursor_mock.execute.call_count)

    def test_load_changed_files_for_commit_no_updates(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        save_extra_files = CommitRepository(database_connection_mock)

        save_extra_files.save_extra_files(single_commit_no_updates_as_json,"git_hub_repo")
        update_call = call("UPDATE GIT_COMMIT "
                           "set lines_added = %s , lines_removed = %s , lines_changed = %s "
                           "where sha = %s",
                           (0,0,0,'67f8beea7ba8f925442f1fbcb34bc9de6e6619ea'))
        cursor_mock.execute.assert_has_calls([update_call])
        self.assertEqual(1, cursor_mock.execute.call_count)

    def test_fetch_all_commits_for_repository_after_date(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        git_database_handler = CommitRepository(database_connection_mock)
        from_date = datetime.datetime(2020, 1, 1, 1, 1, 1)
        git_database_handler.fetch_commits_for_repo("git_repo", date_from=from_date)
        cursor_mock.execute.assert_called_with('SELECT * from GIT_COMMIT where git_repo = %s and updated_at >= %s',("git_repo",from_date))
        cursor_mock.fetchall.assert_called_once()

    def test_fetch_all_commits_for_repository_all_dates(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        git_database_handler = CommitRepository(database_connection_mock)
        git_database_handler.fetch_commits_for_repo("git_repo", None)
        cursor_mock.execute.assert_called_with('SELECT * from GIT_COMMIT where git_repo = %s',("git_repo"))
        cursor_mock.fetchall.assert_called_once()


    def test_fetch_last_updated_date_for_commit(self):
        cursor_mock, database_connection_mock = database_cursor_mock()
        load_commits = CommitRepository(database_connection_mock)
        load_commits.fetch_last_updated_date_for_commit("sha")
        cursor_mock.execute.assert_called_with('SELECT MAX(updated_at) from GIT_COMMIT')
        cursor_mock.fetchone.assert_called_once()