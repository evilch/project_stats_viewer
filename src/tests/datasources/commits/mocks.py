from unittest.mock import MagicMock




# Mock Classes
class Response:
    def __init__(self, status_code, content, headers=None):
        self.status_code = status_code
        self.content = content
        self.headers = headers


class MockSession:
    def __init__(self, auth=None):
        self.auth = auth


def request_mock_returns_200( body, headers=None, ):
    session = MagicMock(name="Session Mock")
    session.get.return_value = response_object(200, body=body, headers=headers)
    return session

def response_object( code, body, headers=None):
    return Response(status_code=code, content=body, headers=headers)



def database_cursor_mock():
    database_connection_mock = MagicMock(name="Database Mock")
    cursor_mock = MagicMock(name="Cursor Mock")
    database_connection_mock.cursor().__enter__.return_value = cursor_mock
    return cursor_mock, database_connection_mock