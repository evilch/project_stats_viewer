import json

octacat_commit = '''
    {
                    "url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                    "sha": "CATb09b5b57875f334f61aebed695e2e4193db5e",
                    "node_id": "MDY6Q29tbWl0NmRjYjA5YjViNTc4NzVmMzM0ZjYxYWViZWQ2OTVlMmU0MTkzZGI1ZQ==",
                    "html_url": "https://github.com/octocat/Hello-World/commit/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                    "comments_url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e/comments",
                    "commit": {
                        "url": "https://api.github.com/repos/octocat/Hello-World/git/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                        "author": {
                            "name": "Monalisa Octocat",
                            "email": "support@github.com",
                            "date": "2011-04-14T16:00:49Z"
                        },
                        "committer": {
                            "name": "Monalisa Octocat",
                            "email": "support@github.com",
                            "date": "2011-04-14T16:00:49Z"
                        },
                        "message": "Fix all the bugs",
                        "tree": {
                            "url": "https://api.github.com/repos/octocat/Hello-World/tree/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                            "sha": "6dcb09b5b57875f334f61aebed695e2e4193db5e"
                        },
                        "comment_count": 0,
                        "verification": {
                            "verified": false,
                            "reason": "unsigned",
                            "signature": null,
                            "payload": null
                        }
                    },
                    "author": {
                        "login": "octocat",
                        "id": 1,
                        "node_id": "MDQ6VXNlcjE=",
                        "avatar_url": "https://github.com/images/error/octocat_happy.gif",
                        "gravatar_id": "",
                        "url": "https://api.github.com/users/octocat",
                        "html_url": "https://github.com/octocat",
                        "followers_url": "https://api.github.com/users/octocat/followers",
                        "following_url": "https://api.github.com/users/octocat/following{/other_user}",
                        "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
                        "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
                        "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
                        "organizations_url": "https://api.github.com/users/octocat/orgs",
                        "repos_url": "https://api.github.com/users/octocat/repos",
                        "events_url": "https://api.github.com/users/octocat/events{/privacy}",
                        "received_events_url": "https://api.github.com/users/octocat/received_events",
                        "type": "User",
                        "site_admin": "false"
                    },
                    "committer": {
                        "login": "octocat",
                        "id": 1,
                        "node_id": "MDQ6VXNlcjE=",
                        "avatar_url": "https://github.com/images/error/octocat_happy.gif",
                        "gravatar_id": "",
                        "url": "https://api.github.com/users/octocat",
                        "html_url": "https://github.com/octocat",
                        "followers_url": "https://api.github.com/users/octocat/followers",
                        "following_url": "https://api.github.com/users/octocat/following{/other_user}",
                        "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
                        "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
                        "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
                        "organizations_url": "https://api.github.com/users/octocat/orgs",
                        "repos_url": "https://api.github.com/users/octocat/repos",
                        "events_url": "https://api.github.com/users/octocat/events{/privacy}",
                        "received_events_url": "https://api.github.com/users/octocat/received_events",
                        "type": "User",
                        "site_admin": false
                    },
                    "parents": [
                        {
                            "url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                            "sha": "6dcb09b5b57875f334f61aebed695e2e4193db5e"
                        }
                    ]
                }

'''
octadog_commit = '''
             {
                    "url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                    "sha": "DOGb09b5b57875f334f61aebed695e2e4193db5e",
                    "node_id": "MDY6Q29tbWl0NmRjYjA5YjViNTc4NzVmMzM0ZjYxYWViZWQ2OTVlMmU0MTkzZGI1ZQ==",
                    "html_url": "https://github.com/octocat/Hello-World/commit/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                    "comments_url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e/comments",
                    "commit": {
                        "url": "https://api.github.com/repos/octocat/Hello-World/git/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                        "author": {
                            "name": "Monalisa Octocat 2",
                            "email": "support@github.com",
                            "date": "2011-04-14T16:00:49Z"
                        },
                        "committer": {
                            "name": "Monalisa Octocat 2",
                            "email": "support@github.com",
                            "date": "2011-04-14T16:00:49Z"
                        },
                        "message": "Fix all the bugs",
                        "tree": {
                            "url": "https://api.github.com/repos/octocat/Hello-World/tree/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                            "sha": "6dcb09b5b57875f334f61aebed695e2e4193db5e"
                        },
                        "comment_count": 0,
                        "verification": {
                            "verified": false,
                            "reason": "unsigned",
                            "signature": null,
                            "payload": null
                        }
                    },
                    "author": {
                        "login": "octocat",
                        "id": 1,
                        "node_id": "MDQ6VXNlcjE=",
                        "avatar_url": "https://github.com/images/error/octocat_happy.gif",
                        "gravatar_id": "",
                        "url": "https://api.github.com/users/octocat",
                        "html_url": "https://github.com/octocat",
                        "followers_url": "https://api.github.com/users/octocat/followers",
                        "following_url": "https://api.github.com/users/octocat/following{/other_user}",
                        "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
                        "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
                        "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
                        "organizations_url": "https://api.github.com/users/octocat/orgs",
                        "repos_url": "https://api.github.com/users/octocat/repos",
                        "events_url": "https://api.github.com/users/octocat/events{/privacy}",
                        "received_events_url": "https://api.github.com/users/octocat/received_events",
                        "type": "User",
                        "site_admin": "false"
                    },
                    "committer": {
                        "login": "octocat",
                        "id": 1,
                        "node_id": "MDQ6VXNlcjE=",
                        "avatar_url": "https://github.com/images/error/octocat_happy.gif",
                        "gravatar_id": "",
                        "url": "https://api.github.com/users/octocat",
                        "html_url": "https://github.com/octocat",
                        "followers_url": "https://api.github.com/users/octocat/followers",
                        "following_url": "https://api.github.com/users/octocat/following{/other_user}",
                        "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
                        "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
                        "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
                        "organizations_url": "https://api.github.com/users/octocat/orgs",
                        "repos_url": "https://api.github.com/users/octocat/repos",
                        "events_url": "https://api.github.com/users/octocat/events{/privacy}",
                        "received_events_url": "https://api.github.com/users/octocat/received_events",
                        "type": "User",
                        "site_admin": false
                    },
                    "parents": [
                        {
                            "url": "https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e",
                            "sha": "6dcb09b5b57875f334f61aebed695e2e4193db5e"
                        }
                    ]
                }
'''

single_commit = r"""
{
  "sha": "67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "node_id": "C_kwDOK5pL6toAKDY3ZjhiZWVhN2JhOGY5MjU0NDJmMWZiY2IzNGJjOWRlNmU2NjE5ZWE",
  "commit": {
    "author": {
      "name": "James Coxhead",
      "email": "james.coxhead@capgemini.com",
      "date": "2024-05-31T16:36:59Z"
    },
    "committer": {
      "name": "GitHub",
      "email": "noreply@github.com",
      "date": "2024-05-31T16:36:59Z"
    },
    "message": "389962: Project user form UI fixes (#140)\n\n* fix: UI for create project users form\r\n\r\n* fix: add title to permissions",
    "tree": {
      "sha": "55446b566d96559e97f7c8fb97a0d1ee2671db8f",
      "url": "https://api.github.com/repos/DEFRA/adp-portal/git/trees/55446b566d96559e97f7c8fb97a0d1ee2671db8f"
    },
    "url": "https://api.github.com/repos/DEFRA/adp-portal/git/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
    "comment_count": 0,
    "verification": {
      "verified": true,
      "reason": "valid",
      "signature": "-----BEGIN PGP SIGNATURE-----\n\nwsFcBAABCAAQBQJmWfyrCRC1aQ7uu5UhlAAAV90QABTBxwKz8A3qlsaul0av4KgA\ntNUAMASPR+c/c+6yUHTtXmOyTUtJDQkM/mi6bBxRXOsAT3dRR5ATZMve7/6cjbbr\nFj/2L475EjURSTwJ8XrUuW3GasxXrAxs4j7bdo4nwav05taCDz+aeVJOgb8mcEyU\nr2CJ1jhVCKtXNGUqF+NJKzex4yzjLSzneRip8UvS+VifCG5UWKmJT8FH43h6Fi9p\nfsCIXOBmLk1zFeOGd5zg/FaJ65Sz4bSoEDDDJysaB410cbkwWjywVksn3pSREJt/\nytXvqlHPQhjvS04C4D3ch9qzgv7VoqMNREZUbSDajwjQV0q1U0BXqs3mrpyiGbJ9\nZLe01sFDOFd6na82rsoptGNdYRUMxf0mIXc0n3avCEF556Ce0bU7Z04tG01K5k3z\nwN2AOEc0M5mGGdl4fy693hwuFgOgDyool/ZdnZV4qrcP7BdqSd1eyVS3I8MpFl70\nx/UigISSZXkA2aa6QRJF336M4qsEmZwC+z+7LDP+UaYnoh1X3G/tGc/ZBUwRY3eg\nCVUsAZHDtW8Xks3+TflQ4RLdQWCXT5lNcOxTAtMq77cQGqRo7db7P8TY3IurRohX\nlkmxOpart7iy2416JTBN8onW1yCGo+mnY6F+Mxp1OZqL2no4Brr+uNpRM5SHlXmz\nvcD8eDiIVs5lcyEAsFQG\n=x+Hl\n-----END PGP SIGNATURE-----\n",
      "payload": "tree 55446b566d96559e97f7c8fb97a0d1ee2671db8f\nparent b9f0aa20fd35e59086682596dd9f4d3896b7980f\nauthor James Coxhead \u003Cjames.coxhead@capgemini.com\u003E 1717173419 +0100\ncommitter GitHub \u003Cnoreply@github.com\u003E 1717173419 +0100\n\n389962: Project user form UI fixes (#140)\n\n* fix: UI for create project users form\r\n\r\n* fix: add title to permissions"
    }
  },
  "url": "https://api.github.com/repos/DEFRA/adp-portal/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "html_url": "https://github.com/DEFRA/adp-portal/commit/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "comments_url": "https://api.github.com/repos/DEFRA/adp-portal/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/comments",
  "author": {
    "login": "jamescoxheadcapgemini",
    "id": 118891549,
    "node_id": "U_kgDOBxYkHQ",
    "avatar_url": "https://avatars.githubusercontent.com/u/118891549?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jamescoxheadcapgemini",
    "html_url": "https://github.com/jamescoxheadcapgemini",
    "followers_url": "https://api.github.com/users/jamescoxheadcapgemini/followers",
    "following_url": "https://api.github.com/users/jamescoxheadcapgemini/following{/other_user}",
    "gists_url": "https://api.github.com/users/jamescoxheadcapgemini/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jamescoxheadcapgemini/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jamescoxheadcapgemini/subscriptions",
    "organizations_url": "https://api.github.com/users/jamescoxheadcapgemini/orgs",
    "repos_url": "https://api.github.com/users/jamescoxheadcapgemini/repos",
    "events_url": "https://api.github.com/users/jamescoxheadcapgemini/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jamescoxheadcapgemini/received_events",
    "type": "User",
    "site_admin": false
  },
  "committer": {
    "login": "web-flow",
    "id": 19864447,
    "node_id": "MDQ6VXNlcjE5ODY0NDQ3",
    "avatar_url": "https://avatars.githubusercontent.com/u/19864447?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/web-flow",
    "html_url": "https://github.com/web-flow",
    "followers_url": "https://api.github.com/users/web-flow/followers",
    "following_url": "https://api.github.com/users/web-flow/following{/other_user}",
    "gists_url": "https://api.github.com/users/web-flow/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/web-flow/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/web-flow/subscriptions",
    "organizations_url": "https://api.github.com/users/web-flow/orgs",
    "repos_url": "https://api.github.com/users/web-flow/repos",
    "events_url": "https://api.github.com/users/web-flow/events{/privacy}",
    "received_events_url": "https://api.github.com/users/web-flow/received_events",
    "type": "User",
    "site_admin": false
  },
  "parents": [
    {
      "sha": "b9f0aa20fd35e59086682596dd9f4d3896b7980f",
      "url": "https://api.github.com/repos/DEFRA/adp-portal/commits/b9f0aa20fd35e59086682596dd9f4d3896b7980f",
      "html_url": "https://github.com/DEFRA/adp-portal/commit/b9f0aa20fd35e59086682596dd9f4d3896b7980f"
    }
  ],
  "stats": {
    "total": 80,
    "additions": 63,
    "deletions": 17
  },
  "files": [
    {
      "sha": "0ec6a10af5ee944eb7bf7c51bedd7d7ebca95878",
      "filename": "app/package.json",
      "status": "modified",
      "additions": 1,
      "deletions": 1,
      "changes": 2,
      "blob_url": "https://github.com/DEFRA/adp-portal/blob/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fpackage.json",
      "raw_url": "https://github.com/DEFRA/adp-portal/raw/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fpackage.json",
      "contents_url": "https://api.github.com/repos/DEFRA/adp-portal/contents/app%2Fpackage.json?ref=67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
      "patch": "@@ -1,6 +1,6 @@\n {\n   \"name\": \"root\",\n-  \"version\": \"1.3.13\",\n+  \"version\": \"1.3.14\",\n   \"private\": true,\n   \"engines\": {\n     \"node\": \"18 || 20\"}"
    },
    {
      "sha": "0decc16ae63d1a3927814710273019bd143e29c8",
      "filename": "app/plugins/adp/src/components/DeliveryProjectUser/AddProjectUserButton.tsx",
      "status": "modified",
      "additions": 6,
      "deletions": 2,
      "changes": 8,
      "blob_url": "https://github.com/DEFRA/adp-portal/blob/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FAddProjectUserButton.tsx",
      "raw_url": "https://github.com/DEFRA/adp-portal/raw/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FAddProjectUserButton.tsx",
      "contents_url": "https://api.github.com/repos/DEFRA/adp-portal/contents/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FAddProjectUserButton.tsx?ref=67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
      "patch": "@@ -7,7 +7,7 @@ import {\n   emptyForm,\n   type DeliveryProjectUserFields,\n } from './DeliveryProjectUserFormFields';\n-import { DialogForm, readValidationError } from '../../utils';\n+import { DialogForm, TitleWithHelp, readValidationError } from '../../utils';\n import type { SubmitResult } from '../../utils';\n import { usePermission } from '@backstage/plugin-permission-react';\n import { deliveryProjectUserCreatePermission } from '@internal/plugin-adp-common';\n@@ -73,7 +73,11 @@ export function AddProjectUserButton({\n             setIsModalOpen(false);\n             if (success) onCreated?.();\n           }}\n-          title=\"Add Team Member\"\n+          title={\n+            \u003CTitleWithHelp href=\"https://defra.github.io/adp-documentation/Getting-Started/onboarding-a-user/\"\u003E\n+              Add team member\n+            \u003C/TitleWithHelp\u003E\n+          }\n           confirm=\"Add\"\n           submit={handleSubmit}\n         /\u003E"
    },
    {
      "sha": "8e6e390761bd40e41f91ad405801c24dce2892ff",
      "filename": "app/plugins/adp/src/components/DeliveryProjectUser/DeliveryProjectUserFormFields.tsx",
      "status": "modified",
      "additions": 10,
      "deletions": 2,
      "changes": 12,
      "blob_url": "https://github.com/DEFRA/adp-portal/blob/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FDeliveryProjectUserFormFields.tsx",
      "raw_url": "https://github.com/DEFRA/adp-portal/raw/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FDeliveryProjectUserFormFields.tsx",
      "contents_url": "https://api.github.com/repos/DEFRA/adp-portal/contents/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FDeliveryProjectUserFormFields.tsx?ref=67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
      "patch": "@@ -53,11 +53,13 @@ export function DeliveryProjectUserFormFields({\n         }}\n       /\u003E\n \n+      \u003Ch3 style={{ marginBottom: 0 }}\u003EAdditional permissions\u003C/h3\u003E\n+\n       \u003CFormCheckboxField\n         control={control}\n         errors={errors}\n         index={i++}\n-        label=\"Admin user?\"\n+        label=\"Admin user\"\n         helperText=\"Can this user administer this project?\"\n         name=\"is_admin\"\n         disabled={disabled}\n@@ -67,7 +69,7 @@ export function DeliveryProjectUserFormFields({\n         control={control}\n         errors={errors}\n         index={i++}\n-        label=\"Technical user?\"\n+        label=\"Technical user\"\n         helperText=\"Is this user in a technical role?\"\n         name=\"is_technical\"\n         disabled={disabled}\n@@ -81,6 +83,12 @@ export function DeliveryProjectUserFormFields({\n         helperText=\"Enter this user's Github username\"\n         name=\"github_username\"\n         disabled={disabled}\n+        rules={{\n+          validate: (value, values) =\u003E\n+            values.is_technical === false ||\n+            (values.is_technical && value) ||\n+            'A GitHub handle is required for technical users',\n+        }}\n       /\u003E\n     \u003C/\u003E\n   );"
    },
    {
      "sha": "015f837778a9b04404ee198491a01d90b94dcdbc",
      "filename": "app/plugins/adp/src/components/DeliveryProjectUser/EditDeliveryProjectUserButton.tsx",
      "status": "modified",
      "additions": 11,
      "deletions": 2,
      "changes": 13,
      "blob_url": "https://github.com/DEFRA/adp-portal/blob/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FEditDeliveryProjectUserButton.tsx",
      "raw_url": "https://github.com/DEFRA/adp-portal/raw/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FEditDeliveryProjectUserButton.tsx",
      "contents_url": "https://api.github.com/repos/DEFRA/adp-portal/contents/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2FEditDeliveryProjectUserButton.tsx?ref=67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
      "patch": "@@ -13,7 +13,12 @@ import {\n   emptyForm,\n } from './DeliveryProjectUserFormFields';\n import type { SubmitResult } from '../../utils';\n-import { DialogForm, populate, readValidationError } from '../../utils';\n+import {\n+  DialogForm,\n+  TitleWithHelp,\n+  populate,\n+  readValidationError,\n+} from '../../utils';\n import { usePermission } from '@backstage/plugin-permission-react';\n \n export type EditDeliveryProjectUserButtonProps = Readonly\u003C\n@@ -82,7 +87,11 @@ export function EditDeliveryProjectUserButton({\n             setIsModalOpen(false);\n             if (success) onEdited?.();\n           }}\n-          title={`Edit team member ${deliveryProjectUser.name}`}\n+          title={\n+            \u003CTitleWithHelp href=\"https://defra.github.io/adp-documentation/Getting-Started/onboarding-a-user/\"\u003E\n+              {`Edit team member ${deliveryProjectUser.name}`}\n+            \u003C/TitleWithHelp\u003E\n+          }\n           confirm=\"Update\"\n           submit={handleSubmit}\n           disabled={{"
    },
    {
      "sha": "beacfaf69ceba10adda466d771e1384772e79433",
      "filename": "app/plugins/adp/src/components/DeliveryProjectUser/__snapshots__/DeliveryProjectUserFormFields.test.tsx.snap",
      "status": "modified",
      "additions": 35,
      "deletions": 10,
      "changes": 45,
      "blob_url": "https://github.com/DEFRA/adp-portal/blob/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2F__snapshots__%2FDeliveryProjectUserFormFields.test.tsx.snap",
      "raw_url": "https://github.com/DEFRA/adp-portal/raw/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2F__snapshots__%2FDeliveryProjectUserFormFields.test.tsx.snap",
      "contents_url": "https://api.github.com/repos/DEFRA/adp-portal/contents/app%2Fplugins%2Fadp%2Fsrc%2Fcomponents%2FDeliveryProjectUser%2F__snapshots__%2FDeliveryProjectUserFormFields.test.tsx.snap?ref=67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
      "patch": "@@ -69,6 +69,11 @@ exports[`DeliveryProjectUserFormFields should render all fields correctly 1`] =\n         Select a user to add to this Delivery Project\n       \u003C/p\u003E\n     \u003C/div\u003E\n+    \u003Ch3\n+      style=\"margin-bottom: 0px;\"\n+    \u003E\n+      Additional permissions\n+    \u003C/h3\u003E\n     \u003Clabel\n       class=\"MuiFormControlLabel-root\"\n     \u003E\n@@ -105,7 +110,7 @@ exports[`DeliveryProjectUserFormFields should render all fields correctly 1`] =\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Admin user?\n+        Admin user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -149,7 +154,7 @@ exports[`DeliveryProjectUserFormFields should render all fields correctly 1`] =\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Technical user?\n+        Technical user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -272,6 +277,11 @@ exports[`DeliveryProjectUserFormFields should render default fields 1`] = `\n         Select a user to add to this Delivery Project\n       \u003C/p\u003E\n     \u003C/div\u003E\n+    \u003Ch3\n+      style=\"margin-bottom: 0px;\"\n+    \u003E\n+      Additional permissions\n+    \u003C/h3\u003E\n     \u003Clabel\n       class=\"MuiFormControlLabel-root\"\n     \u003E\n@@ -308,7 +318,7 @@ exports[`DeliveryProjectUserFormFields should render default fields 1`] = `\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Admin user?\n+        Admin user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -353,7 +363,7 @@ exports[`DeliveryProjectUserFormFields should render default fields 1`] = `\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Technical user?\n+        Technical user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -478,6 +488,11 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Emp\n         Select a user to add to this Delivery Project\n       \u003C/p\u003E\n     \u003C/div\u003E\n+    \u003Ch3\n+      style=\"margin-bottom: 0px;\"\n+    \u003E\n+      Additional permissions\n+    \u003C/h3\u003E\n     \u003Clabel\n       class=\"MuiFormControlLabel-root\"\n     \u003E\n@@ -514,7 +529,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Emp\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Admin user?\n+        Admin user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -558,7 +573,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Emp\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Technical user?\n+        Technical user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -683,6 +698,11 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Git\n         Select a user to add to this Delivery Project\n       \u003C/p\u003E\n     \u003C/div\u003E\n+    \u003Ch3\n+      style=\"margin-bottom: 0px;\"\n+    \u003E\n+      Additional permissions\n+    \u003C/h3\u003E\n     \u003Clabel\n       class=\"MuiFormControlLabel-root\"\n     \u003E\n@@ -719,7 +739,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Git\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Admin user?\n+        Admin user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -763,7 +783,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Git\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Technical user?\n+        Technical user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -888,6 +908,11 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Use\n         Select a user to add to this Delivery Project\n       \u003C/p\u003E\n     \u003C/div\u003E\n+    \u003Ch3\n+      style=\"margin-bottom: 0px;\"\n+    \u003E\n+      Additional permissions\n+    \u003C/h3\u003E\n     \u003Clabel\n       class=\"MuiFormControlLabel-root\"\n     \u003E\n@@ -924,7 +949,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Use\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Admin user?\n+        Admin user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp\n@@ -968,7 +993,7 @@ exports[`DeliveryProjectUserFormFields should write values back to the form: Use\n       \u003Cspan\n         class=\"MuiTypography-root MuiFormControlLabel-label MuiTypography-body1\"\n       \u003E\n-        Technical user?\n+        Technical user\n       \u003C/span\u003E\n     \u003C/label\u003E\n     \u003Cp"
    }
  ]
}
"""

single_commit_no_updates = r"""
{
  "sha": "67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "node_id": "C_kwDOK5pL6toAKDY3ZjhiZWVhN2JhOGY5MjU0NDJmMWZiY2IzNGJjOWRlNmU2NjE5ZWE",
  "commit": {
    "author": {
      "name": "James Coxhead",
      "email": "james.coxhead@capgemini.com",
      "date": "2024-05-31T16:36:59Z"
    },
    "committer": {
      "name": "GitHub",
      "email": "noreply@github.com",
      "date": "2024-05-31T16:36:59Z"
    },
    "message": "389962: Project user form UI fixes (#140)\n\n* fix: UI for create project users form\r\n\r\n* fix: add title to permissions",
    "tree": {
      "sha": "55446b566d96559e97f7c8fb97a0d1ee2671db8f",
      "url": "https://api.github.com/repos/DEFRA/adp-portal/git/trees/55446b566d96559e97f7c8fb97a0d1ee2671db8f"
    },
    "url": "https://api.github.com/repos/DEFRA/adp-portal/git/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
    "comment_count": 0,
    "verification": {
      "verified": true,
      "reason": "valid",
      "signature": "-----BEGIN PGP SIGNATURE-----\n\nwsFcBAABCAAQBQJmWfyrCRC1aQ7uu5UhlAAAV90QABTBxwKz8A3qlsaul0av4KgA\ntNUAMASPR+c/c+6yUHTtXmOyTUtJDQkM/mi6bBxRXOsAT3dRR5ATZMve7/6cjbbr\nFj/2L475EjURSTwJ8XrUuW3GasxXrAxs4j7bdo4nwav05taCDz+aeVJOgb8mcEyU\nr2CJ1jhVCKtXNGUqF+NJKzex4yzjLSzneRip8UvS+VifCG5UWKmJT8FH43h6Fi9p\nfsCIXOBmLk1zFeOGd5zg/FaJ65Sz4bSoEDDDJysaB410cbkwWjywVksn3pSREJt/\nytXvqlHPQhjvS04C4D3ch9qzgv7VoqMNREZUbSDajwjQV0q1U0BXqs3mrpyiGbJ9\nZLe01sFDOFd6na82rsoptGNdYRUMxf0mIXc0n3avCEF556Ce0bU7Z04tG01K5k3z\nwN2AOEc0M5mGGdl4fy693hwuFgOgDyool/ZdnZV4qrcP7BdqSd1eyVS3I8MpFl70\nx/UigISSZXkA2aa6QRJF336M4qsEmZwC+z+7LDP+UaYnoh1X3G/tGc/ZBUwRY3eg\nCVUsAZHDtW8Xks3+TflQ4RLdQWCXT5lNcOxTAtMq77cQGqRo7db7P8TY3IurRohX\nlkmxOpart7iy2416JTBN8onW1yCGo+mnY6F+Mxp1OZqL2no4Brr+uNpRM5SHlXmz\nvcD8eDiIVs5lcyEAsFQG\n=x+Hl\n-----END PGP SIGNATURE-----\n",
      "payload": "tree 55446b566d96559e97f7c8fb97a0d1ee2671db8f\nparent b9f0aa20fd35e59086682596dd9f4d3896b7980f\nauthor James Coxhead \u003Cjames.coxhead@capgemini.com\u003E 1717173419 +0100\ncommitter GitHub \u003Cnoreply@github.com\u003E 1717173419 +0100\n\n389962: Project user form UI fixes (#140)\n\n* fix: UI for create project users form\r\n\r\n* fix: add title to permissions"
    }
  },
  "url": "https://api.github.com/repos/DEFRA/adp-portal/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "html_url": "https://github.com/DEFRA/adp-portal/commit/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea",
  "comments_url": "https://api.github.com/repos/DEFRA/adp-portal/commits/67f8beea7ba8f925442f1fbcb34bc9de6e6619ea/comments",
  "author": {
    "login": "jamescoxheadcapgemini",
    "id": 118891549,
    "node_id": "U_kgDOBxYkHQ",
    "avatar_url": "https://avatars.githubusercontent.com/u/118891549?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jamescoxheadcapgemini",
    "html_url": "https://github.com/jamescoxheadcapgemini",
    "followers_url": "https://api.github.com/users/jamescoxheadcapgemini/followers",
    "following_url": "https://api.github.com/users/jamescoxheadcapgemini/following{/other_user}",
    "gists_url": "https://api.github.com/users/jamescoxheadcapgemini/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jamescoxheadcapgemini/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jamescoxheadcapgemini/subscriptions",
    "organizations_url": "https://api.github.com/users/jamescoxheadcapgemini/orgs",
    "repos_url": "https://api.github.com/users/jamescoxheadcapgemini/repos",
    "events_url": "https://api.github.com/users/jamescoxheadcapgemini/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jamescoxheadcapgemini/received_events",
    "type": "User",
    "site_admin": false
  },
  "committer": {
    "login": "web-flow",
    "id": 19864447,
    "node_id": "MDQ6VXNlcjE5ODY0NDQ3",
    "avatar_url": "https://avatars.githubusercontent.com/u/19864447?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/web-flow",
    "html_url": "https://github.com/web-flow",
    "followers_url": "https://api.github.com/users/web-flow/followers",
    "following_url": "https://api.github.com/users/web-flow/following{/other_user}",
    "gists_url": "https://api.github.com/users/web-flow/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/web-flow/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/web-flow/subscriptions",
    "organizations_url": "https://api.github.com/users/web-flow/orgs",
    "repos_url": "https://api.github.com/users/web-flow/repos",
    "events_url": "https://api.github.com/users/web-flow/events{/privacy}",
    "received_events_url": "https://api.github.com/users/web-flow/received_events",
    "type": "User",
    "site_admin": false
  },
  "parents": [
    {
      "sha": "b9f0aa20fd35e59086682596dd9f4d3896b7980f",
      "url": "https://api.github.com/repos/DEFRA/adp-portal/commits/b9f0aa20fd35e59086682596dd9f4d3896b7980f",
      "html_url": "https://github.com/DEFRA/adp-portal/commit/b9f0aa20fd35e59086682596dd9f4d3896b7980f"
    }
  ],
  "stats": {
  },
  "files": [
  ]
}
"""
## Extra methods to create sets of data.

response = '[' + octacat_commit + ',' + octadog_commit + ']'

octacat_commit_as_json = json.loads(octacat_commit)
octadog_commit_as_json = json.loads(octadog_commit)
full_response = json.loads(response)
single_commit_as_json = json.loads(single_commit)
single_commit_no_updates_as_json = json.loads(single_commit_no_updates)

