import unittest
from unittest.mock import MagicMock

from external.datasources.clients.client_github import GitHubClient
from external.datasources.pull_requests.PullRequestImport import map_github_pull_request_to_internal_model, \
    PullRequestImport
from external.datasources.releases.release_import import ReleaseDataImport, map_github_release_to_internal_model
from tests.datasources.commits.mocks import request_mock_returns_200
from tests.datasources.pull_requests.pull_request_test_data import pull_requests, pull_requests_json


class PullRequestTest(unittest.TestCase):

    pull_request_data = {
        "id": 1296269,
        "repo_id": "Hello-World",
        "title": "Amazing new feature",
        "state": "open",
        "url": "https://api.github.com/repos/octocat/Hello-World/pulls/1347",
        "created_at": "2011-01-26T19:01:12Z",
        "updated_at": "2011-01-26T19:01:12Z",
        "closed_at": "2011-01-26T19:01:12Z",
        "author": "octocat",
        "reviewers_count": 1,
        "assignees_count": 2
    }

    def test_pull_request_load(self):
        # Mock git hub client
        session = request_mock_returns_200(body=pull_requests)
        git_client = GitHubClient(session)
        database_connection_mock = MagicMock(name="Database Mock")
        pull_request_importer = PullRequestImport(database_connection_mock, git_client)
        pull_request_importer.import_pull_requests("http://host:2003", "octocat", "Hello-World")
        session.get.assert_called_with(url="http://host:2003/octocat/Hello-World/pulls?state=all")
        database_connection_mock.save_pull_request.assert_called_with(self.pull_request_data)

    def test_map_github_pull_request_to_internal_model(self):
        internal_model = map_github_pull_request_to_internal_model(pull_requests_json[0], 'Hello-World')
        self.assertEqual(self.pull_request_data, internal_model)


if __name__ == '__main__':
    unittest.main()
