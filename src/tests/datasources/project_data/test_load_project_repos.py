import json
import unittest
from unittest.mock import MagicMock, call

from external.datasources.clients.json_file_loader import JsonFileImporter
from external.datasources.project_data.ProjectRepoImporter import ProjectRepoImporter
from tests.datasources.project_data.project_data_test import project1, project2, project3, repo1, repo2, repo3


class ProjectRepoLoadTestCase(unittest.TestCase):
    def test_load_projects(self):
        json_file_importer = JsonFileImporter()
        repository_mock = MagicMock(name="Database Mock")
        project_repo_import = ProjectRepoImporter(json_file_importer, repository_mock)
        project_repo_import.load_projects("tests/datasources/project_data/projects.json")

        self.assertEqual(repository_mock.save_project.call_count, 3)
        project_save_calls = [call(project1), call(project2), call(project3)]
        repository_mock.save_project.assert_has_calls(project_save_calls)

    def test_load_repos(self):
        json_file_importer = JsonFileImporter()
        repository_mock = MagicMock(name="Database Mock")
        project_repo_import = ProjectRepoImporter(json_file_importer, repository_mock)
        project_repo_import.load_repos("tests/datasources/project_data/repositories.json")

        self.assertEqual(repository_mock.save_repo.call_count, 3)
        repo_save_calls = [call(repo1), call(repo2), call(repo3)]
        repository_mock.save_repo.assert_has_calls(repo_save_calls)


if __name__ == '__main__':
    unittest.main()
