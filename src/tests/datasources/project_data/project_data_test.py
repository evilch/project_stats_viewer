
project1 = {
    "id": 1,
    "name": "Project A",
    "description": "This is project A",
    "url_to_details": "https://projectA.com/details"
}
project2 = {
    "id": 2,
    "name": "Project B",
    "description": "Description for project B",
    "url_to_details": "https://projectB.com/details"
}
project3 = {
    "id": 3,
    "name": "Project C",
    "description": "Project C details",
    "url_to_details": "https://projectC.com/details"
}

project_data = '''[
    {
        "id": 1,
        "name": "Project A",
        "description": "This is project A",
        "url_to_details": "https://projectA.com/details"
    },
    {
        "id": 2,
        "name": "Project B",
        "description": "Description for project B",
        "url_to_details": "https://projectB.com/details"
    },
    {
        "id": 3,
        "name": "Project C",
        "description": "Project C details",
        "url_to_details": "https://projectC.com/details"
    }
]'''

repo1 = {
        "id": 1,
        "name": "Repo X",
        "description": "Repository X description",
        "url_to_details": "https://repoX.com/details",
        "project_id": 1
    }

repo2 = {
        "id": 2,
        "name": "Repo Y",
        "description": "Description for repository Y",
        "url_to_details": "https://repoY.com/details",
        "project_id": 2
    }

repo3 = {
        "id": 3,
        "name": "Repo Z",
        "description": "Details for repository Z",
        "url_to_details": "https://repoZ.com/details",
        "project_id": 3
    }
