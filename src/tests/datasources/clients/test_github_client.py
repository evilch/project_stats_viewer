import unittest
from unittest.mock import MagicMock, call

from external.datasources.clients.client_github import format_commit_url, format_release_url, GitHubClient
from tests.datasources.commits.git_hub_test_data import response, full_response
from tests.datasources.commits.mocks import response_object


class GitHubClientTest(unittest.TestCase):
    header = {
        'link': '<https://api.github.com/repositories/1300192/issues?page=514>; rel="prev",'
                ' <https://api.github.com/repositories/1300192/issues?page=516>; rel="next",'
                ' <https://api.github.com/repositories/1300192/issues?page=576>; rel="last",'
                ' <https://api.github.com/repositories/1300192/issues?page=1>; rel="first"'}
    header_no_next = {
        'link': '<https://api.github.com/repositories/1300192/issues?page=514>; rel="prev",'
                ' <https://api.github.com/repositories/1300192/issues?page=576>; rel="last",'
                ' <https://api.github.com/repositories/1300192/issues?page=1>; rel="first"'
    }
    def test_parse_response_from_github(self):

        http_response = response_object(200, body=response, headers=self.header)
        github_client = GitHubClient()
        parsed_response = github_client.parse_response(http_response)
        self.assertEqual(parsed_response['first'], 'https://api.github.com/repositories/1300192/issues?page=1')
        self.assertEqual(parsed_response['last'], 'https://api.github.com/repositories/1300192/issues?page=576')
        self.assertEqual(parsed_response['next'], 'https://api.github.com/repositories/1300192/issues?page=516')
        self.assertEqual(parsed_response['prev'], 'https://api.github.com/repositories/1300192/issues?page=514')
        self.assertEqual(parsed_response['content'], full_response)

    def test_paginated_url_call(self):
        data_base_call_method = MagicMock(name="CommitDataBaseHandler")
        session = MagicMock(name="Session Mock")
        session.get.side_effect = [response_object(200,
                                                   body='''[{"name":"bob1"}, {"name":"bob2"}, {"name":"bob3"}]''',
                                                   headers=self.header),
                                   response_object(200,
                                                   body='''[{"name":"alice1"}, {"name":"alice2"}, {"name":"alice3"}]''',
                                                   headers=self.header),
                                   response_object(200,
                                                   body='''[{"name":"sue"}]''',
                                                   headers=self.header_no_next)]


        git_hub_client = GitHubClient(session)
        git_hub_client.load_with_paging("https://api.github.com/org/git_hub_repo/commits",
                                        data_base_call_method)


        # Assume we have 3 pages of 5 commits
        self.assertEqual(7, data_base_call_method.call_count)
        data_base_call_method.assert_has_calls( [
            call({'name':'bob1'}),
            call({'name':'bob2'}),
            call({'name':'bob3'}),
            call({'name':'alice1'}),
            call({'name':'alice2'}),
            call({'name':'alice3'}),
            call({'name':'sue'})

        ])


if __name__ == '__main__':
    unittest.main()
