import unittest

from external.datasources.clients.client_github import format_commit_url,format_release_url





class FormatGitUrlTest(unittest.TestCase):
    def test_basic_commit(self):
        url = format_commit_url("https://api.github.com/repos", "DEFRA", "adp-portal")
        self.assertEqual ("https://api.github.com/repos/DEFRA/adp-portal/commits", url)

    def test_basic_commit_with_from_date(self):
        params = {"since": "2021-01-01"}
        url = format_commit_url("https://api.github.com/repos", "DEFRA", "adp-portal", params)
        self.assertEqual ("https://api.github.com/repos/DEFRA/adp-portal/commits?since=2021-01-01", url)

    def test_basic_commit_with_from_date_with_100_per_page(self):
        params = {"since": "2021-01-01", "per_page": "100"}
        url = format_commit_url("https://api.github.com/repos", "DEFRA", "adp-portal", params)
        self.assertEqual("https://api.github.com/repos/DEFRA/adp-portal/commits?since=2021-01-01&per_page=100", url)

    def test_basic_release_with_date_per_page(self):
        params = {"page": "2", "per_page": "100"}
        url = format_release_url("https://api.github.com/repos", "DEFRA", "adp-portal", params)
        self.assertEqual("https://api.github.com/repos/DEFRA/adp-portal/releases?page=2&per_page=100", url)
if __name__ == '__main__':
    unittest.main()
