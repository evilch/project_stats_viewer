import datetime
import unittest
import psycopg

from external.datasources.commits.commit_repository import CommitRepository
from external.datasources.project_data.ProjectRepoRepository import ProjectRepoRepository
from external.datasources.pull_requests.PullRequestRepository import PullRequestRepository
from external.datasources.releases.release_repository import ReleaseRepository
from tests.datasources.commits.git_hub_test_data import octacat_commit_as_json, octadog_commit_as_json, \
    single_commit_as_json, single_commit_no_updates_as_json


def execute_database_scripts(script_name, cursor):
    with open(script_name, 'r') as script:
        cursor.execute(script.read())


class DatabaseConnectionTests(unittest.TestCase):

    def setUpClass():
        with psycopg.connect(
                "user=stats port=5432 host=localhost password=stats ") as connection:  # psycopg.connect("user=stats port=5432 host=localhost password=stats ")
            with connection.cursor() as cursor:
                try:
                    connection.autocommit = True
                    cursor.execute("create database test")
                except psycopg.errors.DuplicateDatabase:
                    # Database already exists
                    pass
        # Run set up Scripts
        with psycopg.connect(
                "dbname=test user=stats port=5432 host=localhost password=stats ") as connection:  # psycopg.connect("user = psycopg.connect("dbname test user=stats port=5432 host=localhost password=stats ")
            # Create database base on scripts
            with connection.cursor() as cursor:
                execute_database_scripts('../backend/postgres/01_schema.sql', cursor)
                execute_database_scripts('../backend/postgres/02_add_git_repo_column.sql', cursor)
                execute_database_scripts('../backend/postgres/03_changes_columns.sql', cursor)
                execute_database_scripts('../backend/postgres/04_extra_columns_to_git_commit.sql', cursor)
                execute_database_scripts('../backend/postgres/05_release_data.sql', cursor)
                execute_database_scripts('../backend/postgres/06_pull_request_data.sql', cursor)
                execute_database_scripts('../backend/postgres/07_project_data.sql', cursor)

    def tearDownClass():
        with psycopg.connect(
                "user=stats port=5432 host=localhost password=stats ") as connection:  # psycopg.connect("user=stats port=5432 host=localhost password=stats ")
            with connection.cursor() as cursor:
                connection.autocommit = True
                cursor.execute("drop database test")

    def setUp(self):
        self.connection = psycopg.connect("dbname=test user=stats port=5432 host=localhost password=stats ")

    def tearDown(self):
        self.connection.close()

    def test_save_multiple_commits(self):

        database_handler = CommitRepository(self.connection)
        database_handler.map_and_save(octacat_commit_as_json, "DEFRA", "adp-portal")
        database_handler.map_and_save(octadog_commit_as_json, "DEFRA", "adp-portal")
        database_handler.map_and_save(single_commit_as_json, "DEFRA", "adp-portal")
        database_handler.map_and_save(single_commit_no_updates_as_json, "DEFRA", "adp-portal")
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT count(*) from GIT_COMMIT")
            self.assertEqual(3, cursor.fetchone()[0])

    def test_add_release_data(self):
        release_data = {
            "component_name": "adp-portal",
            "release_name": "v0.0.1",
            "release_description": "v0.0.1",
            "release_date": "2021-01-01",
            "source_system": "github",
            "author": "author",
            "link_to_details": "https://github.com/DEFRA/adp-portal/releases/tag/v0.0.1"}

        release_database_handler = ReleaseRepository(self.connection)
        release_database_handler.save_release(release_data)

        release_data = release_database_handler.fetch_release("adp-portal-v0.0.1")

        self.assertEqual("adp-portal", release_data[1])
        self.assertEqual("v0.0.1", release_data[2])
        self.assertEqual("v0.0.1", release_data[3])
        self.assertEqual(datetime.datetime(2021, 1, 1, 0, 0), release_data[4])
        self.assertEqual("github", release_data[5])
        self.assertEqual("author", release_data[6])
        self.assertEqual("https://github.com/DEFRA/adp-portal/releases/tag/v0.0.1", release_data[7])

    def test_save_pull_request_data(self):

        pull_request_data = {
            "id": 1296269,
            "repo_id": "repo",
            "title": "test",
            "state": "open",
            "url": "https://github.com/DEFRA/adp-portal/pull/1",
            "created_at": "2021-01-01 00:00:00",
            "updated_at": "2021-01-01 00:00:00",
            "open_issues": "1",
            "closed_issues": "1",
            "closed_at": "2021-01-01 00:00:00",
            "author": "author"
        }

        pull_request_repository = PullRequestRepository(self.connection)
        pull_request_repository.save_pull_request(pull_request_data)

        pull_request = pull_request_repository.fetch_pull_request(1296269)
        self.assertEqual("repo", pull_request[1])
        self.assertEqual("test", pull_request[2])
        self.assertEqual("open", pull_request[3])
        self.assertEqual("https://github.com/DEFRA/adp-portal/pull/1", pull_request[4])
        self.assertEqual(datetime.datetime(2021, 1, 1, 0, 0), pull_request[5])
        self.assertEqual(datetime.datetime(2021, 1, 1, 0, 0), pull_request[6])
        self.assertEqual(datetime.datetime(2021, 1, 1, 0, 0), pull_request[7])
        self.assertEqual("author", pull_request[8])

    def test_save_project_data(self):

        project_test_data = {
            "id": 1,
            "name": "Project A",
            "description": "This is project A",
            "url_to_details": "https://projectA.com/details"
        }

        project_repository = ProjectRepoRepository(self.connection)
        project_repository.save_project(project_test_data)

        repository = project_repository.fetch_project(1)
        self.assertEqual("Project A", repository[1])
        self.assertEqual("This is project A", repository[2])
        self.assertEqual("https://projectA.com/details", repository[3])

        # Test save repository data based on above project

        repo_test_data = {
            "id": 1,
            "name": "Repo X",
            "description": "Repository X description",
            "url_to_details": "https://repoX.com/details",
            "project_id": 1
        }

        repo_repository = ProjectRepoRepository(self.connection)
        repo_repository.save_repo(repo_test_data)

        repository = repo_repository.fetch_repo(1)
        self.assertEqual("Repo X", repository[1])
        self.assertEqual("Repository X description", repository[2])
        self.assertEqual("https://repoX.com/details", repository[3])
        self.assertEqual(1, repository[4])


if __name__ == '__main__':
    unittest.main()
