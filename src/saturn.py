# create a main method for this python file.
# main method is called from main.py
import argparse

import psycopg
import logging
import requests

from external.datasources.clients.json_file_loader import JsonFileImporter
from external.datasources.commits.import_commits import GitHubImporter
from external.datasources.clients.client_github import GitHubClient
from external.datasources.commits.commit_repository import CommitRepository
from external.datasources.project_data.ProjectRepoImporter import ProjectRepoImporter
from external.datasources.project_data.ProjectRepoRepository import ProjectRepoRepository
from external.datasources.pull_requests.PullRequestImport import PullRequestImport
from external.datasources.pull_requests.PullRequestRepository import PullRequestRepository
from external.datasources.releases.release_import import ReleaseDataImport
from external.datasources.releases.release_repository import ReleaseRepository

github_base_url = "https://api.github.com/repos"

logger = logging.getLogger(__name__)
db_connection_string = "dbname=stats user=external_stats_user port=5432 host=localhost password=external_stats_user "

logging.basicConfig(level=logging.INFO)


def load_commits(conn, new_args):
    importer = GitHubImporter(CommitRepository(conn), create_github_client())
    importer.load_commits_from_repo(github_base_url,new_args.git_org, new_args.git_repo)


def enhance_commits(conn, new_args):
    importer = GitHubImporter(CommitRepository(conn), create_github_client())
    importer.enhance_commits_from_repo(new_args.git_repo)


def load_releases(conn, new_args):
    importer = ReleaseDataImport(ReleaseRepository(conn), create_github_client())
    importer.import_releases(github_base_url, new_args.git_org, new_args.git_repo)


def load_pull_requests( conn, new_args):
    importer = PullRequestImport(PullRequestRepository(conn), create_github_client())
    importer.import_pull_requests(github_base_url, new_args.git_org, new_args.git_repo)


def load_project_data(conn, new_args):
    logger.info("Loading project data")
    json_file_importer = JsonFileImporter()
    importer = ProjectRepoImporter(json_file_importer, ProjectRepoRepository(conn))
    logger.info("Loading projects")
    importer.load_projects(new_args.project_data_dir + "/projects.json")
    logger.info("Loading Repositories")
    importer.load_repos(new_args.project_data_dir + "/repositories.json")


def create_github_client():
    session = requests.session()
    github_client = GitHubClient(session)
    return github_client


def with_db_connection(func):
    def wrapper(pargs):
        with psycopg.connect(db_connection_string) as conn:
            func(conn, pargs)

    return wrapper


def open_db_connection_and_run_action(parsed_args):
    with_db_connection(parsed_args.func)(parsed_args)


def add_github_arguments(parser):

    parser.add_argument('--git_org',    action='store', help='Git Hub organisation', required=True)
    parser.add_argument('--git_repo', action='store', help='Git Hub repository', required=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='SATURN',
        description='SPACE Analysis and Tracking for Ultimate Reporting Needs',
        epilog='Text at the bottom of help',
        formatter_class=argparse.RawTextHelpFormatter)



    action_parsers = parser.add_subparsers(dest='action', help='Available actions')
    load_pull_requests_parser = action_parsers.add_parser('load_pull_requests')
    add_github_arguments(load_pull_requests_parser)

    load_pull_requests_parser.set_defaults(func=load_pull_requests)
    load_commits_parser = action_parsers.add_parser('load_commits')
    add_github_arguments(load_commits_parser)

    load_commits_parser.set_defaults(func=load_commits)
    load_releases_parser = action_parsers.add_parser('load_releases')
    add_github_arguments(load_releases_parser)
    load_releases_parser.set_defaults(func=load_releases)

    enhance_commits_parser = action_parsers.add_parser('enhance_commits')
    enhance_commits_parser.add_argument('--repo', action='store', help='Git Hub repo to load', required=True)
    enhance_commits_parser.set_defaults(func=enhance_commits)

    load_project_data_parser = action_parsers.add_parser('load_project_data')
    load_project_data_parser.add_argument('--project_data_dir', action='store', help='Directory storing '
                                                                                     'projects.json, '
                                                                                     'and repositories.json',
                                          required=True)
    load_project_data_parser.set_defaults(func=load_project_data)

    args = parser.parse_args()
    logger.info("Parsed args {}".format(args))
    open_db_connection_and_run_action(args)
