# Project statistics viewer

## Project description
TODO

 

## Architecture
--Diagram to go here--

### Components

Backend : Grafana with a postgres database.

Schema: Defined schema for data

external: A set of functions that scrape data from various sources, and convert/load them into the schema (ETL)

## Getting started
### Pre-requisites
- python 3 or higher
- podman (or other OCI container runner)

#### activate the venv
```
    source ./venv/bin/activate
```

#### install requirements
```
    pip install -r requirements.txt
```

#### run the app
```
    TODO
```
#### start the backend
```
   cd backend
   podman compose up -f compose.yaml 
```

#### Update the schema
```shell
 TODO
```
--- you can run it in manually using the Sql files in the backend/postgres folder.



