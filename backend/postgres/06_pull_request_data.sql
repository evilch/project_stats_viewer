

CREATE TABLE IF NOT EXISTS PULL_REQUEST (
    id BIGINT NOT NULL PRIMARY KEY,
    repo_id varchar(255) NOT NULL,
    title TEXT NOT NULL,
    state varchar(255) NOT NULL,
    url TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NULL,
    closed_at timestamp NULL ,
    author varchar(255) NOT NULL
);
