CREATE TABLE IF NOT EXISTS PROJECT (
    id BIGINT NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    description TEXT  NULL,
    url_to_details varchar(255)  NULL
);

CREATE TABLE IF NOT EXISTS REPOSITORY (
   id BIGINT NOT NULL PRIMARY KEY,
   name varchar(255) NOT NULL,
   description TEXT NOT NULL,
   url_to_details varchar(255) NOT NULL,
   project_id BIGINT NOT NULL REFERENCES PROJECT(id)
);
