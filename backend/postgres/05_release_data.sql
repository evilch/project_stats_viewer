

CREATE TABLE IF NOT EXISTS RELEASE (
    composite_id varchar(255) primary key, -- composite of component_name and release_name
    component_name varchar(100) not null,
    release_name varchar(100),
    release_description text not null,
    release_date timestamp not null,
    source_system varchar(255) not null,
    author varchar(255) not null,
    link_to_details varchar(255)
);
