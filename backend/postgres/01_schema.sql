-- use the sche for the stats database
CREATE TABLE IF NOT EXISTS GIT_REPO (
    name varchar(100) primary key,
    url varchar(255) not null,
    sshUrl varchar(255) not null,
    last_updated timestamp not null,
    created timestamp not null
);

CREATE TABLE IF NOT EXISTS GIT_COMMIT (
    sha varchar(40) primary key,
    short_sha varchar(10) not null,
    author_name varchar(255) not null,
    author_email varchar(255) not null,
    message text not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

CREATE TABLE IF NOT EXISTS GIT_FILES
(
    file_id   varchar(255) primary key,
    file_name varchar(255) not null,
    git_repo  varchar(100) not null,
    commit    varchar(40)  not null
);

