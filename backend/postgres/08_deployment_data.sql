CREATE TABLE IF NOT EXISTS DEPLOYMENT (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    description TEXT  NULL,
    date_deployed timestamp NOT NULL,
    environment varchar(255) NOT NULL,
    url_to_details varchar(255)  NULL
);

CREATE TABLE IF NOT EXISTS BUILD (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    description TEXT  NULL,
    date_built timestamp NOT NULL,
    repository varchar(255) NOT NULL,
    url_to_details varchar(255)  NULL
);
