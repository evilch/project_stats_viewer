alter table git_commit add column lines_added integer;
alter table git_commit add column lines_removed integer;
alter table git_commit add column lines_changed integer;

alter table git_files add column lines_added integer;
alter table git_files add column lines_removed integer;
alter table git_files add column lines_changed integer;