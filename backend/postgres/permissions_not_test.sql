-- Need to grant permissions after the tables are created (not sure why?)
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA stats TO external_stats_user;
GRANT SELECT ON ALL TABLES IN SCHEMA stats TO grafana_stats;