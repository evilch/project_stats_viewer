-- create a stats database  with schema stats and a single user
CREATE schema stats;

ALTER DATABASE stats SET search_path TO stats;
-- Create a grafana stats user that has readonly access to the stats database
CREATE USER grafana_stats WITH PASSWORD 'grafana_stats';
GRANT CONNECT ON DATABASE stats TO grafana_stats;
GRANT USAGE ON SCHEMA stats TO grafana_stats;
GRANT SELECT ON ALL TABLES IN SCHEMA stats TO grafana_stats;
-- Create an external_stats_user that has write access to any tables in the stats database, but no create privileges

CREATE USER external_stats_user WITH PASSWORD 'external_stats_user';
GRANT CONNECT ON DATABASE stats TO external_stats_user;
GRANT USAGE ON SCHEMA stats TO external_stats_user;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA stats TO external_stats_user;




